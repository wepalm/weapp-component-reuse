# 微信小程序行程线路&展示类 横向滑动列表【复用】

### 更新概述
`2020-08-21`
1. 修正WXML中文字超长时超出的问题
2. 调整了2个列表的图片输入渲染数据字段
3. 调整了大图展示列表中的价格输入渲染字段   

*_（**注意NPM更新时会替换被修改过的渲染字段，如需手动修改请参阅GIT比对）_*

`...`
* 早期的提交更改

---

#### Gitee 同步仓库
> [仓库地址：https://gitee.com/wepalm/weapp-component-reuse](https://gitee.com/wepalm/weapp-component-reuse)
##### Issue 更好的建议或bug报告      
> [提交 Issue](https://gitee.com/wepalm/weapp-component-reuse/issues)
##### PR 更好的开发完善建议     
> [提交 PR](https://gitee.com/wepalm/weapp-component-reuse/pulls)
---

## 组件使用说明

>依赖：         
>1、@Vant 组件      
>2、weapp-global-wxss       
>3、colorui

```
依赖 @Vant-Weapp   
> npm i @vant/weapp -S --production    
```
```
依赖 weapp-global-wxss
npm i weapp-global-wxss -S --production
该npm组件同自有发布组件（未完结）
```

引用的@Vant组件
```json
"usingComponents": {
    "van-image": "@vant/weapp/image/index",
    "van-loading": "@vant/weapp/loading/index",
    "van-rate": "@vant/weapp/rate/index"
}
```

引用weapp-global-wxss【在合适的地方引用，或修改组件引用信息】
```css
@import "YOUR_PATH_TO/global.wxss";
@import "YOUR_PATH_TO/fa-icon.wxss";
@import "YOUR_PATH_TO/icon.wxss";
```

> 安装后构建NPM后即可直接使用, 构建后的目录自行参考微信小程序开发文档说明   

页面配置 page.json
- 组件Tag请勿与官方组件同名
```
{
  "usingComponents": {
    "some-list": "YOUR_PATH/weapp-tour-list/tourList",
  }
}
```

### 组件默认不展示任何图文列表方式
> 使用 xxxShow 与 传入数据 tourList 结构展示

- options `样式隔离 isolated`

参数 | 说明 | 类型 | 默认 
---|---|---|---
title | 标题 | string | 行程精选
more-text | 更多文字 | string | 更多
more-pagepath | 更多跳转页面, 仅当传入该参数时点击态有效 | string | pending
tour-list | 数据列表 | any | pending
page-path | 跳转详情页面,由小程序页面而定 | string | pending
tour-show | 小图展示 | boolean | false
act-show | 大图展示，带推荐指数等 | boolean | false

### 组件提供小图&大图的展示方式 wx:for 结构见wxml文件
`可传入如下结构的数据列表体验`

> tourShow="true" tourList结构
```
tourList: [
    {
        id: 0,
        image: "url/images.jpg",
        title: "祖安3日游",
        location: "祖安"
    }
]
```

> actShow="true" tourList结构
```
actList: [
    {
        id: 0,
        image: "url/images.jpg",
        title: "面对疾风吧！",
        location: "祖安",
        price: 998,
        pay_price: 199,
        end_time: '1970-01-01',
        rate: 4.5,
        count_text: '0.35k'
    }
]
```