Component({
    options:{
        styleIsolation: "isolated"
    },
    properties: {
        title:{
            type: String,
            value:"行程精选"
        },
        moreText:{
            type: String,
            value: "更多"
        },
        morePagepath: String,       //  更多跳转页面  仅当传入该参数时点击态有效
        listRaw: null,              //  数据列表
        pagePath: String,           //  跳转链接
        tourShow: {                 //  展示 行程列表
            type: Boolean,
            value: false
        },
        actShow:  {                 //  展示 活动列表
            type: Boolean,
            value: false
        },
        customClass: String,        //  根节点自定义样式
        customStyle: String,        //  根节点自定义样式
    },
    data: {},
    methods: {
        //  跳转页面
        _onToPage(event: any){
            let { url } = event.currentTarget.dataset;
            wx.navigateTo({
                url: url
            });
        }
    }
});
