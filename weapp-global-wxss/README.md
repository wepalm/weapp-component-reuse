# 全局WXSS样式

> 更新概况 

`2020-08-21`
1. 调整了下划线颜色
2. 微调 .text-color-title-mark .text-color-mark 的颜色，主体色调未做变更
3. 调整 .text-color-white 颜色样式为高优先级（!important)
4. 修正 .pd-b-20 实际样式错误的问题
5. 新增 .pd-tb(lr)-0  .pd-lr-12

`更新样式主体不会影响现有的样式基础，微调部分及新增`

#### Gitee 同步仓库
* [仓库地址：https://gitee.com/wepalm/weapp-component-reuse](https://gitee.com/wepalm/weapp-component-reuse)
##### Issue 更好的建议或bug报告      
* [提交 Issue](https://gitee.com/wepalm/weapp-component-reuse/issues)
##### PR 更好的开发完善建议     
* [提交 PR](https://gitee.com/wepalm/weapp-component-reuse/pulls)
---

## 组件使用说明

> 特别说明：weapp-global-wxss 包体内容  
1. global.wxss 自建的常用样式（未完结；可能很扯；脑补或issue开喷；PR完善）  
2. colorui 框架（主要引入了main.wxss icon.wxss；此外还有animation.wxss未使用）  
`包内：main.wxss; icon.wxss`    
3. fa-icon.wxss font awsome 4.7 ICON图标  
4. 组件中不存在相互依赖于引用，但组件中会引用 weapp-global-wxss 包内容   
```
**着重关注：因发布和开发原因 组件中 wxss 引用的 weapp-global-wxss 路径即为可能存在不符，请自行调整后使用（后期做统一调整，尽量一次 install 后直接使用 或 欢迎 PR 来做调整）
```

### 安装
`npm i weapp-global-wxss`

安装后使用微信开发工具【构建NPM】

> 构建NPM后在 App.wxss 或需要的地方引入
```css
@import "YOUR_PATH_TO/global.wxss";
@import "YOUR_PATH_TO/fa-icon.wxss";
```

> global.wxss   
> 自定义的常用通用样式

1. 定义的全局样式，包含 padding margin 等常用样式
2. 定义了基础部分
3. 具体详见文件内容

#### padding margin 使用方式
```css
/** ### padding 8起, 以及8的倍数
  pd-l-xx padding-left    xx(PX)  左padding
  pd-r-xx padding-top     xx(PX)  右padding
  pd-t-xx padding-top     xx(PX)  上padding
  pd-b-xx padding-bottom  xx(PX)  下padding

  pd-xx padding xx xx xx xx (px)  上下左右padding

  pd-tb-xx padding: xx(px) 0; 上下padding
  pd-lr-xx padding: 0 xx(px); 左右padding
*/
```

> fa-icon.wxss  
> Font Awesome 4.7.0 

提供 Font Awesome 4.7.0 提供的图标样式

use
```html
<text class="fa fa-home"></text>
```