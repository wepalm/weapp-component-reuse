# 微信小程序 通信加密方法

> 通信加密适用于 发送/接收 加解密

[![Wepalm.CN/微信小程序复用组件](https://gitee.com/wepalm/weapp-component-reuse/widgets/widget_card.svg?colors=4183c4,ffffff,ffffff,e3e9ed,666666,9b9b9b)](https://gitee.com/wepalm/weapp-component-reuse)
#### Gitee 同步仓库
> [仓库地址：https://gitee.com/wepalm/weapp-component-reuse](https://gitee.com/wepalm/weapp-component-reuse)
##### Issue 更好的建议或bug报告      
> [提交 Issue](https://gitee.com/wepalm/weapp-component-reuse/issues)
##### PR 更好的开发完善建议     
> [提交 PR](https://gitee.com/wepalm/weapp-component-reuse/pulls)
---

## 组件使用说明

```text
详见包内文件
```

- 使用 TypeScript 开发
- 已同步编译一个相关 js 文件

#### 安装后报错解决方案
- 包内含有 aes.js 文件
- 使用时报错，请检查主文件 secret.ts[js] 引用 aes.js 路径是否正确
```javascript
// 请关注 secret.ts 文件中 引用的路径

const aes = require("./aes.js")
```

必要操作
> 安装后构建NPM        
> 安装后构建NPM        
> 安装后构建NPM    

### 使用时，在需要的地方引用包内容