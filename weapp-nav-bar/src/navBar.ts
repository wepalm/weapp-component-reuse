Component({
    options: {
        styleIsolation: "isolated",     //  样式隔离 组件内外不受其他wxss影响，仅组件内wxss有效
    },
    properties: {
        //  是否启用 返回上一页
        back: {
            type: Boolean,
            value: false
        },
        //  是否启用 返回首页
        home: {
            type: Boolean,
            value: false
        },
        //  设置Title
        title: {
            type: String,
            value: "畔山拾趣"
        },
        //  设置无Title
        noTitle:{
            type: Boolean,
            value: false
        },
        //  NavBar 整体背景颜色
        bgColor: {
            type: null,
            value: "white"
        },
        //  是否使用下边框
        border: {
            type: Boolean,
            value: true
        },
        //  标题颜色
        titleColor: {
            type: String,
            value: "#1A1A1A"
        },
        //  是否启用搜索
        search: {
            type: Boolean,
            value: true
        },
        //  搜索跳转页面
        sLink: {
            type: String,
            value: ''
        },
        //  底部阴影
        shadow: {
            type: Boolean,
            value: true
        }
    },
    data: {
        animationData: {},
        device: wx.getSystemInfoSync(),
        menu: wx.getMenuButtonBoundingClientRect(),
        navHeight: 44,      //  nav高度
    },
    methods: {
        //  返回到首页
        _onToHome() {
            wx.navigateBack({
                delta: 99
            });
        },
        //  退回上一页
        _onBackPage() {
            wx.navigateBack();
        }
    },
    lifetimes: {
        attached() {
            //  组件进入节点树时设置NavBar高度
            if (this.data.device.platform == "android") {
                this.setData({
                    navHeight: 48
                });
            } else {
                this.setData({
                    navHeight: 44
                });
            }
        }
    },
    pageLifetimes: {
        show() {
            //  页面期间加载动画
            let animation = wx.createAnimation({
                duration: 230,
                timingFunction: 'linear',
            });
            animation.scale(1).step();
            this.setData({
                animationData: animation.export()
            });
        }
    }
});
