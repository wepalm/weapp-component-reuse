# 自定义导航栏

[![Wepalm.CN/微信小程序复用组件](https://gitee.com/wepalm/weapp-component-reuse/widgets/widget_card.svg?colors=4183c4,ffffff,ffffff,e3e9ed,666666,9b9b9b)](https://gitee.com/wepalm/weapp-component-reuse)
#### Gitee 同步仓库
> [仓库地址：https://gitee.com/wepalm/weapp-component-reuse](https://gitee.com/wepalm/weapp-component-reuse)
##### Issue 更好的建议或bug报告      
> [提交 Issue](https://gitee.com/wepalm/weapp-component-reuse/issues)
##### PR 更好的开发完善建议     
> [提交 PR](https://gitee.com/wepalm/weapp-component-reuse/pulls)
---

## 组件使用说明
**依赖具有相互性，缺失组件请参阅仓库总体说明，如有未完善请见谅！**

```
依赖
NPM: weapp-global-wxss

INSTALL: npm i weapp-global-wxss -S --production
```

页面配置 或 全局配置
```json
{
  "navigationStyle": "custom",
}
```

> 组件示例展示 拥有 【返回 主页 搜索 标题】     
> ![demo](https://gitee.com/wepalm/weapp-component-reuse/raw/master/weapp-nav-bar/nav-bar-img.png)

> 固定的导航栏高度      
> Android: 48       
> iOS: 44

```
1、导航栏默认只显示标题 【返回、首页、搜索均不显示】
2、导航栏高度固定为 Android = 48；iOS = 44；单位 px
3、返回首页时调用 wx.navigateBack() 设置属性 delta: 99
```

> navBar 导航栏
- options `样式隔离 isolated`
- Props

参数 | 说明 | 类型 | 默认 
---|---|---|---
back         | 返回上页 |  boolean |  false 
home         | 返回首页 |  boolean |  false 
title        | 标题 |  string |  畔山拾趣 
noTitle      | 无标题设置，为true设置title无效 |  boolean |  false 
bgColor      | 根节点背景色,接受任意css样式color值 |  any |  white 
border       | 导航下边框 |  boolean |  true 
titleColor   | 标题文字颜色 |  string |  #1A1A1A 
searchTitle  | 搜索展示文字 |  string |  探索！ 
sLink        | 跳转页面路径，仅当传入该参数才会展示左侧搜索 |  string |  无 
shadow       | 底部阴影 |  boolean |  true 