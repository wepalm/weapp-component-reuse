# NPM包组件说明

```
包体内容不管是否对开发者有帮助无论是否可用 Issue劲情开喷[都接受、不屏蔽]

都接受并欢迎PR Fork

组件为自用发布，如果后期有复用的可能，反正【Issue 可喷】【PR 可喷】【Fork 可喷】【Star 可喷】

接受 All 祖安招呼


PS：LOL祖安老喷子，招呼随意 ^_^
```

> ### 包内无论【组件】或【样式】        
> ***       
>>> 安装后必须使用 微信开发工具 【构建NPM】       
>>> 安装后必须使用 微信开发工具 【构建NPM】       
>>> 安装后必须使用 微信开发工具 【构建NPM】

***

详细包名及使用说明参阅各个包内 `README.md` 文件

```
特别说明：weapp-global-wxss 包体内容
1、global.wxss 自建的常用样式（未完结；可能很扯；脑补或issue开喷；PR完善）

2、colorui 框架（主要引入了main.wxss icon.wxss；此外还有animation.wxss未使用）
包内：main.wxss; icon.wxss

3、fa-icon.wxss font awsome 4.7 ICON图标

4、组件中不存在相互依赖于引用，但组件中 wxss 会引用 weapp-global-wxss 包内容做样式展现
```

***

#### NPM包内组件
> 全局通用样式       
>> weapp-global-wxss

> 自定义导航        
>> weapp-nav-bar

> 轮播组件      
>> weapp-swiper     

> 两种尺寸的图文横滑列表
>> weapp-tour-list


## Gitee同步仓库（仅公开）     

[公开同步：NPM & Gitee](https://gitee.com/wepalm/weapp-component-reuse)

- 组件内如有未完善的说明依赖都请参阅具体引用或直接此仓库提供的组件信息      
- 目前，组件间不存在相互依赖，组件中的依赖仅存在于第三方
- ** 若第三方组件对您没有用，你仍可以 install 第三方组件后，移除对组件没有帮助的冗余组件，自行将组件重新引用，以免对您的小程序增加包体容量

## 样式缺失解决方案

> 目前包内组件的wxss所引用 @import 不为NPM构建的包路径, 您需要手动调整引用路径, 后期会做调整，使其直接可用

包内组件若样式缺失 可能是缺失 weapp-global-wxss 包导致

- 安装wxss样式依赖，构建NPM，并调整组件内wxss的引用路径可解决
```npm
npm i weapp-global-wxss -S --production
```