# 微信小程序轮播组件【复用】

[![Wepalm.CN/微信小程序复用组件](https://gitee.com/wepalm/weapp-component-reuse/widgets/widget_card.svg?colors=4183c4,ffffff,ffffff,e3e9ed,666666,9b9b9b)](https://gitee.com/wepalm/weapp-component-reuse)
#### Gitee 同步仓库
> [仓库地址：https://gitee.com/wepalm/weapp-component-reuse](https://gitee.com/wepalm/weapp-component-reuse)
##### Issue 更好的建议或bug报告      
> [提交 Issue](https://gitee.com/wepalm/weapp-component-reuse/issues)
##### PR 更好的开发完善建议     
> [提交 PR](https://gitee.com/wepalm/weapp-component-reuse/pulls)
---

## 组件使用说明

> 组件示例展示【高度自定义，不同方式展现轮播】      
> ![demo](https://gitee.com/wepalm/weapp-component-reuse/raw/master/weapp-swiper/swiper-img.png)

```
依赖：
1、官方swiper组件
2、@Vant/weapp/image 组件
```
> @Vant-Weapp   
> INSTALL: npm i @vant/weapp -S --production

#### 安装
```
npm i weapp-swiper
```

> 安装后构建NPM后即可直接使用, 构建后的目录自行参考微信小程序开发文档说明   

页面配置 page.json
> 组件Tag名请勿使用 swiper 否则和官方组件重名，导致参数配置失败
```
{
  "usingComponents": {
    "swipers": "YOUR_PATH/weapp-swiper/swiper",
  }
}
```

#### 组件参数属说明

##### 支持运行的 数据列表 基础结构
```json
{
    "id": 1,
    "title": "Title",
    "subtitle": "SubTitle",
    "description": "Description",
    "image": "IMG_URL_PATH",
    "pagepath": "Weapp PagePath",
    "target": "self | miniProgram",
    "appid": "APPID",
    "app_version": "trial | release | develop"
}
```
> 无需声明跳转名单，不限跳转数量（众测中）  
> [官方说明](https://developers.weixin.qq.com/miniprogram/dev/api/open-api/miniprogram-navigate/wx.navigateToMiniProgram.html#%E6%97%A0%E9%9C%80%E5%A3%B0%E6%98%8E%E8%B7%B3%E8%BD%AC%E5%90%8D%E5%8D%95%EF%BC%8C%E4%B8%8D%E9%99%90%E8%B7%B3%E8%BD%AC%E6%95%B0%E9%87%8F%EF%BC%88%E4%BC%97%E6%B5%8B%E4%B8%AD%EF%BC%89)
>
> 从2020年4月24日起，使用跳转其他小程序功能将无需在全局配置中声明跳转名单，调用此接口时将不再校验所跳转的 AppID 是否在 navigateToMiniProgramAppIdList 中。
>   
> 从2020年4月24日起，跳转其他小程序将不再受数量限制，使用此功能时请注意遵守运营规范。

- options `共享App.wxss样式 apply-shared`
- Props
```
Component({
    properties: {
        // ...
    }
})
```
- 自定义组件参数

参数 | 说明 | 类型 | 默认 
---|---|---|---
swiperHeight | swiper高度，接受任意css单位，或数值 | any [number, string] | 220px
swiperList | 数据列表 | any | pending
swiperList.pagepath | 数据列表，页面路径，数据中有该属性则可跳转到指定页面 | string | pending
noText | 轮播图片浮层文字，为true则彻底不显示 | Boolean | false
customStyle | 自定义style | any | 无
customClass | 自定义外部class | any | 无
imgMode | Vant图片组件MODE方式 | string | cover
imgRadius | Vant图片圆角大小 | any | 0

- swiper默认参数

参数 | 说明 | 类型 | 默认 
---|---|---|---
autoplay | 自动播放 | true | true
dot | 展示切换焦点 | boolean | true
circular | 无缝切换 | boolean | false
vertical | 垂直切换 | boolean | false
interval | 轮播切换时间 | number | 5000