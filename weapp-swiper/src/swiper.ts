Component({
    options:{
        styleIsolation: "apply-shared",     //  共享主要css
    },
    properties: {
        swiperHeight:{
            type: null,
            value: "220px",
            observer: function (newVal: any) {
                //  传入的是纯数字
                if(!isNaN(newVal)){
                    this.setData({
                        swiperHeight: newVal + "px"
                    })
                }
                //  如果不是纯数字则按照 style 特征使用，单位包括但不限于px
            }
        },
        swiperList:{
            type: null,
            value: ""
        },
        //  swiper图片浮层文字 彻底不显示 no-text: true
        noText:{
            type: Boolean,
            value: false
        },

        customStyle: null,  //  自定义属性，同步 style
        customClass: null,  //  自定义属性，同步 class
        imgMode:{
            type: String,
            value: "cover"
        },
        imgRadius:{
            type: null,
            value: ""
        },
        /**
         * 官方组件 swiper 属性保持默认属性特征
         */
        //  自动播放 除该属性不使用默认值以外
        autoplay:{
            type: Boolean,
            value: true
        },
        //  展示切换点标识
        dot: {
            type: Boolean,
            value: true
        },
        //  无缝切换
        circular:{
            type: Boolean,
            value: false
        },
        //  swiper垂直切换
        vertical:{
            type: Boolean,
            value: false
        },
        //  切换时间
        interval: {
            type: Number,
            value: 5000
        }
    },
    data: {},
    methods: {
        _onToPage(event: any){
            let { url, target, appid, app_version } = event.currentTarget.dataset;
            switch (target) {
                case "self":
                    //  自身跳转
                    wx.navigateTo({
                        url: url
                    })
                    break;
                case "miniProgram":
                    //  跳转到小程序
                    wx.navigateToMiniProgram({
                        appId: appid,
                        path: url || "",
                        envVersion: app_version,
                    })
                    break;
            }
        }
    }
});
